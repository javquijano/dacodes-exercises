<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'user'], function () use ($router) {// Users CRUD
        $router->get('',  ['uses' => 'UserController@showAllUsers']);
        $router->get('{id}', ['uses' => 'UserController@showOneUser']);
        $router->post('', ['uses' => 'UserController@create', 'middleware' => 'auth']);
        $router->delete('{id}', ['uses' => 'UserController@delete', 'middleware' => 'auth']);
        $router->put('{id}', ['uses' => 'UserController@update', 'middleware' => 'auth']);
    });
    $router->group(['prefix' => 'course'], function () use ($router) { // Courses CRUD
        $router->get('',  ['uses' => 'CourseController@showAllCourses']);
        $router->get('{id}', ['uses' => 'CourseController@showOneCourse']);
        $router->post('', ['uses' => 'CourseController@create', 'middleware' => 'auth']);
        $router->delete('{id}', ['uses' => 'CourseController@delete', 'middleware' => 'auth']);
        $router->put('{id}', ['uses' => 'CourseController@update', 'middleware' => 'auth']);
        $router->get('student/{id}', ['uses' => 'UserController@getAvailableCourses']);
    });
    $router->group(['prefix' => 'lesson'], function () use ($router) { // Lessons CRUD
        $router->get('',  ['uses' => 'LessonController@showAllLessons']);
        $router->get('{id}', ['uses' => 'LessonController@showOneLesson']);
        $router->post('', ['uses' => 'LessonController@create', 'middleware' => 'auth']);
        $router->delete('{id}', ['uses' => 'LessonController@delete', 'middleware' => 'auth']);
        $router->put('{id}', ['uses' => 'LessonController@update', 'middleware' => 'auth']);
        $router->get('details/{id}',  ['uses' => 'LessonController@getAllDetails']);
        $router->post('answers',  ['uses' => 'LessonController@takeLesson']);
        $router->get('student/{id_user}/{id_course}', ['uses' => 'UserController@getAvailableLessons']);
    });
    $router->group(['prefix' => 'question'], function () use ($router) { // Questions CRUD
        $router->get('',  ['uses' => 'QuestionController@showAllQuestions']);
        $router->get('{id}', ['uses' => 'QuestionController@showOneQuestion']);
        $router->post('', ['uses' => 'QuestionController@create', 'middleware' => 'auth']);
        $router->delete('{id}', ['uses' => 'QuestionController@delete', 'middleware' => 'auth']);
        $router->put('{id}', ['uses' => 'QuestionController@update', 'middleware' => 'auth']);
    });
    $router->group(['prefix' => 'answer '], function () use ($router) { // Answers CUD
        $router->post('', ['uses' => 'AnswerController@create', 'middleware' => 'auth']);
        $router->delete('{id}', ['uses' => 'AnswerController@delete', 'middleware' => 'auth']);
        $router->put('{id}', ['uses' => 'AnswerController@update', 'middleware' => 'auth']);
    });
    $router->group(['prefix' => 'enrollment'], function () use ($router) { // list users current courses and lessons

    });
});
