# Courses API 

API description

## Requirements

- PHP >= 7.2
- Enable the following PHP extensions.
    - OpenSSL
    - PDO
    - Mbstring

## Installation

1. Clone API repository into server.
2. Duplicate and rename .env.example to .env and configure the APP and DB variables inside the new .env file.
3. Run the following commands on the folder where the "artisan" file is located.
4. Run "php artisan migrate" (to generate the base tables for the database)
5. Run "php artisan db:seed" (to generate the needed data for the API)  
    1. Run "php artisan dp:seed --class=TestDataSeeder" for including test data in the database (Optional, NOT recommended for PRODUCTION environment).

## Usage

Every petition works with the prefix "api" in the uri followed by the object you want to work 
with:
- course
- lesson
- question
- answer
- user

Example: https://domain.com/api/course  

-------

##### The general CRUD operations work as it follows:  
- Get all the entries of the desired object.  
    - GET /api/(object)
- Get a unique entry of the desired object.  
    - GET /api/(object)/(object_id)  
- Create a new entry of the desired object.  
    - POST /api/(object)  
        - The body of the POST petition must include all of the fields and their respective values for the database entry.  
- Update an entry of the desired object.  
    - PUT /api/(object)/(object_id)  
- Delete an entry of the desired object.  
    - DELETE /api/(object)/(object_id)  
    
##### POST fields and requirements:  
- date format "Y-m-d H:i:s"
- /api/course  
    - course_name : string  
    - parent_course : integer  
    - api_token : string (only users of type professor have this api_token)  
    - created_at : date  
    - updated_at : date  
- /api/lesson
    - parent_lesson : integer  
    - id_course : integer  
    - lesson_name : string  
    - approval_score : integer (0 - 100)  
    - api_token : string (only users of type professor have this api_token)  
    - created_at : date  
    - updated_at : date   
- /api/question  
    - id_course : integer  
    - id_lesson : integer  
    - id_type : integer (1 - 4, check question types)  
    - question : string  
    - score : integer (0 - 100)  
    - created_at : date  
    - updated_at : date   
- /api/answer  
    - id_question : integer  
    - answer_text : string  
    - answer_value : integer (0 - 1)  
    - created_at : date  
    - updated_at : date  
- /api/user  
    - api_token : string (only users of type professor should have this attribute)  
    - email : email  
    - id_user_type : integer (1 - 2)  
    - password : string  
    - user_firstname : string  
    - user_lastname : string  
    - created_at : date  
    - updated_at : date  
    
##### Specific API operations
- GET /api/lesson/details/(lesson_id)  
    - get all required information for taking a lesson, this includes questions and answers of the lesson selected via the lesson_id parameter.
- POST /api/lesson/answers
    - send all the answers of the lesson taken by the student. This endpoint takes the following information.  
        - id_lesson : integer
        - id_user : integer
        - answers : JSON (see below for structure). 
- GET /api/course/student/(student_id)
    - list all available courses to the student selected using the student_id parameter.
- GET /api/lesson/student/(student_id)
    - list all available lessons to the student selected using the student_id parameter.

###### Answers JSON structure  
```
{   
    (id_question): {    
        "answer": string (the string representation of the selected answer),  
        "result": boolean (boolean representing if the answer was correct or incorrect)  
    },  
    (id_question): {  
        "answer": string,  
        "result": boolean  
    },  
}
```

## Utilized Frameworks

- [Lumen](https://lumen.laravel.com/) a framework focused mainly in API development or 
lightweight webapps, this framework was chosed based on the following qualities.
    - API response speed 
        - Since this framework is extremely lightweight and doesn't take into account 
        some of the unnecessary laravel features (for this use case), the processing speed 
        of the petitions will be lower compared to other frameworks.
    - Laravel's usefulness  
        - Laravel contains multiple classes, libraries and functions useful for API development
        Lumen maintains all of these so that the development team can utilize them, the intuitiveness
        and convenience of Laravel without the weight of the unused or less useful functions 
        (for this use case).

