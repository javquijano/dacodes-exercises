<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Answer extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'Answers';
    protected $primaryKey = 'id_answer';
    protected $fillable = [
        'id_question', 'answer_text', 'answer_value', 'created_at', 'updated_at'
    ];

    public function question(){
        $this->belongsTo('App\Question');
    }
}
