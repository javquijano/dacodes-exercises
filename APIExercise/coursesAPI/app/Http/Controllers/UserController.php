<?php


namespace App\Http\Controllers;


use App\Course;
use App\Lesson;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function showAllUsers(){
        return response()->json(User::all());
    }

    public function showOneUser($id){
        return response()->json(User::find($id));
    }

    public function create(Request $request){
        $this->validate($request, [
            'user_firstname' => 'required',
            'user_lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'id_user_type' => 'required',
            'created_at' => 'required',
            'updated_at' => 'required'
        ]);
        $enc_pass = Crypt::encrypt($request->password);
        $request->merge(['password' => $enc_pass]);
        $user = User::create($request->all());
        return response()->json($user, 201);
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'email' => 'email|unique:users',
            'updated_at' => 'required',
        ]);
        $user = User::findOrFail($id);
        $user->update($request->all());
        return response()->json($user, 200);
    }

    public function delete($id){
        User::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function getAvailableCourses($id){
        $user = User::findOrFail($id);
        $enrolled_courses = $user->enrolled_courses;
        return response()->json(Course::getAvailableCoursesUser($enrolled_courses), 200);
    }

    public function getAvailableLessons($id_user, $id_course){
        $user = User::findOrFail($id_user);
        $course = Course::findOrFail($id_course);
        $lesson_records = $user->lesson_records;
        $lessons_course = $course->lessons;
        $lessons_record_course = [];
        foreach($lesson_records as $record){
            if($record->id_course == $id_course){
                array_push($lessons_record_course, $record);
            }
        }
        return response()->json(Lesson::getAvailableLessonsUser($lessons_record_course, $lessons_course), 200);

    }

}
