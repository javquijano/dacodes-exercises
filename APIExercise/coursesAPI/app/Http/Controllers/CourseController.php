<?php


namespace App\Http\Controllers;


use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function showAllCourses(){
        return response()->json(Course::all());
    }

    public function showOneCourse($id){
        return response()->json(Course::find($id));
    }

    public function create(Request $request){
        $this->validate($request, [
            'course_name' => 'required',
            'parent_course' => 'required',
            'created_at' => 'required',
            'updated_at' => 'required',
        ]);
        $course = Course::create($request->all());
        return response()->json($course, 201);
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'updated_at' => 'required',
        ]);
        $course = Course::findOrFail($id);
        $course->update($request->all());
        return response()->json($course, 200);
    }

    public function delete($id){
        Course::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
