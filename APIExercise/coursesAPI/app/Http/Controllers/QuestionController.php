<?php


namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function showAllQuestions(){
        return response()->json(Question::all());
    }

    public function showOneQuestion($id){
        return response()->json(Question::find($id));
    }

    public function create(Request $request){
        $this->validate($request, [
            'question' => 'required',
            'id_lesson' => 'required',
            'id_course' => 'required',
            'id_type' => 'required',
            'score' => 'required|digits_between:0,100',
            'created_at' => 'required',
            'updated_at' => 'required',
        ]);
        $lesson = Question::create($request->all());
        return response()->json($lesson, 201);
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'approval_score' => 'digits_between:0,100',
            'updated_at' => 'required',
        ]);
        $lesson = Lesson::findOrFail($id);
        $lesson->update($request->all());
        return response()->json($lesson, 200);
    }

    public function delete($id){
        Lesson::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
