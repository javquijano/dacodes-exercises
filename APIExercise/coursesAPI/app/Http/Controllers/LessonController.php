<?php


namespace App\Http\Controllers;

use App\Course;
use App\Lesson;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function showAllLessons(){
        return response()->json(Lesson::all());
    }

    public function showOneLesson($id){
        return response()->json(Lesson::find($id));
    }

    public function getAllDetails($id){
        $lesson = Lesson::findOrFail($id);
        $lesson->questions = $lesson->questions();
        foreach ($lesson->questions as $index => $question){
            $lesson->questions[$index] = $question->answers();
        }
        return response()->json($lesson, 200);
    }

    public function takeLesson(Request $request){
        $this->validate($request, [
            'answers' => 'required|json',
            'id_user' => 'required',
            'id_lesson' => 'required'
        ]);
        $lesson = Lesson::findOrFail($request->id_lesson);
        $questions = json_decode($request->answers, true);
        $lesson_questions = $lesson->questions;
        if(sizeof($lesson_questions) != sizeof($questions)){
            abort(500, 'all questions must be answered before finishing the lesson');
        }
        $current_score = 0;
        foreach($questions as $index => $question){
            if($question['result']){
                foreach ($lesson_questions as $lesson_question){
                    $current_score += ($lesson_question->id_question == $index) ? $lesson_question->score : 0;
                }
            }
        }
        $result = [
            'id_course' => $lesson->id_course,
            'current_score' => $current_score,
            'status' => ($current_score >= $lesson->approval_score) ? 2 : 3
        ];
        $lesson->user_records()->attach($request->id_user, $result);
        Course::verify_course_status($lesson->id_course, $request->id_user);
        return response()->json($result);
    }

    public function create(Request $request){
        $this->validate($request, [
            'lesson_name' => 'required',
            'parent_lesson' => 'required',
            'id_course' => 'required',
            'approval_score' => 'required|digits_between:0,100',
            'created_at' => 'required',
            'updated_at' => 'required',
        ]);
        $lesson = Lesson::create($request->all());
        return response()->json($lesson, 201);
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'approval_score' => 'digits_between:0,100',
            'updated_at' => 'required',
        ]);
        $lesson = Lesson::findOrFail($id);
        $lesson->update($request->all());
        return response()->json($lesson, 200);
    }

    public function delete($id){
        Lesson::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function getAvailableLessons($id){

    }
}
