<?php


namespace App\Http\Controllers;

use App\Answer;
use App\Lesson;
use Illuminate\Http\Request;


class AnswerController extends Controller
{
    public function getAllAnswers($id){
        $lesson = Lesson::findOrFail($id);
        $answers = array();
        foreach ($lesson->questions() as $index => $question) {
            $answers[$index] = $question->answers();
        }
         return response()->json($answers, 200);
    }

    public function create(Request $request){
        $this->validate($request, [
            'question' => 'required',
            'id_lesson' => 'required',
            'id_course' => 'required',
            'id_type' => 'required',
            'score' => 'required|digits_between:0,100',
            'created_at' => 'required',
            'updated_at' => 'required',
        ]);
        $answer = Answer::create($request->all());
        return response()->json($answer, 201);
    }

    public function update($id, Request $request){
        $this->validate($request, [
            'approval_score' => 'digits_between:0,100',
            'updated_at' => 'required',
        ]);
        $answer = Answer::findOrFail($id);
        $answer->update($request->all());
        return response()->json($answer, 200);
    }

    public function delete($id){
        Answer::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
