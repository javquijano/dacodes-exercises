<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Question_type extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;
    protected $table = 'Question_types';
    protected $primaryKey = 'id_type';
    protected $fillable = [
        'type_name', 'created_at', 'updated_at'
    ];

    public function questions(){
        return $this->hasMany('App/Question');
    }
}
