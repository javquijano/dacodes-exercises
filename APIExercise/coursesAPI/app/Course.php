<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;

class Course extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'Courses';
    protected $primaryKey = 'id_course';
    protected $fillable = [
        'course_name', 'parent_course', 'created_at', 'updated_at'
    ];

    public function lessons(){
        return $this->hasMany('App\Lesson', 'id_course', 'id_course');
    }
    public static function verify_course_status($id_course, $id_user){
        $course_lessons = DB::table('lessons')
            ->where('id_course', $id_course)
            ->get();

        $lessons_passed= DB::table('lessons_record_students')
            ->where('id_course', $id_course)
            ->where('status', 2);

        $update_array = ["status" => 1, "created_at" => date("Y-m-d H:i:s"), "updated_at" => date("Y-m-d H:i:s")];
        if(sizeof($course_lessons) == sizeof($lessons_passed)){
            $update_array['status'] = 2;
        }
        DB::table('courses_record_students')->updateOrInsert(
            ['id_course'=> $id_course, "id_user" => $id_user],
            $update_array
        );
    }

    public static function firstCourse(){
        return DB::table('courses')->first();
    }

    public static function getAvailableCoursesUser($enrolledCourses){
        $all_courses = DB::table('courses')->get();
        $available_courses_ids = [];
        foreach($all_courses as $course){
            if($course->parent_course == null){
                array_push($available_courses_ids, $course->id_course);
            }
            if(isset($enrolledCourses) && isset($enrolledCourses[0])){
                foreach($enrolledCourses as $enrolled){
                    if($course->id_course == $enrolled->id_course){
                        array_push($available_courses_ids, $course->id_course);
                    }
                    if($course->parent_course != null && $course->parent_course == $enrolled->id_course){
                        array_push($available_courses_ids, $course->id_course);
                    }
                }
            }
        }
        $available_courses_ids = array_unique($available_courses_ids);
        $available_courses = [];
        foreach($all_courses as $course){
            foreach ($available_courses_ids as $id){
                if($course->id_course == $id){
                    array_push($available_courses, $course);
                }
            }
        }
        return $available_courses;
    }
}
