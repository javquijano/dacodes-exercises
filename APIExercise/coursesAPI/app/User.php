<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'Users';
    protected $primaryKey = 'id_user';
    protected $fillable = [
        'user_firstname', 'user_lastname', 'email', 'id_user_type', 'created_at', 'updated_at', 'password'
    ];

    public function user_type(){
        return $this->belongsTo('App\User_type');
    }
    public function lesson_records(){
        return $this->belongsToMany('App\Lesson', 'lessons_record_students', 'id_user', 'id_lesson')
            ->withPivot('id_course','status','current_score')->withTimestamps();
    }
    public function enrolled_courses(){
        return $this->belongsToMany('App\Course', 'courses_record_students', 'id_user', 'id_course')
            ->withpivot('status')->withTimestamps();
    }
}
