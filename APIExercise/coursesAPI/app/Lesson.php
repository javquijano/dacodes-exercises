<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Auth\Authorizable;

class Lesson extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'Lessons';
    protected $primaryKey = 'id_lesson';
    protected $fillable = [
        'parent_lesson', 'id_course', 'lesson_name', 'approval_score', 'created_at', 'updated_at'
    ];

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
    public function questions()
    {
        return $this->hasMany('App\Question', 'id_lesson');
    }
    public function user_records()
    {
        return $this->belongsToMany('App\User', 'lessons_record_students', 'id_lesson', 'id_user')->withPivot('id_course','status','current_score')->withTimestamps();
    }

    public static function firstLesson($id_course){
        return DB::table('lessons')->where('id_course', $id_course)->first();
    }

    public static function getAvailableLessonsUser($lessons_record_course, $lessons_course){
        $available_lessons_ids = [];
        foreach($lessons_course as $lesson){
            if($lesson->parent_lesson == null){
                array_push($available_lessons_ids, $lesson->id_lesson);
            }
            if(isset($lessons_record_course) && sizeof($lessons_record_course) > 0){
                foreach($lessons_record_course as $enrolled){
                    if($lesson->id_lesson == $enrolled->id_lesson){
                        array_push($available_lessons_ids, $lesson->id_lesson);
                    }
                    if($lesson->parent_lesson != null && $lesson->parent_lesson == $enrolled->id_lesson){
                        array_push($available_lessons_ids, $lesson->id_lesson);
                    }
                }
            }
        }
        $available_lessons_ids = array_unique($available_lessons_ids);
        $available_lessons = [];
        foreach($lessons_course as $lesson){
            foreach ($available_lessons_ids as $id){
                if($lesson->id_lesson == $id){
                    array_push($available_lessons, $lesson);
                }
            }
        }
        return $available_lessons;
    }

}
