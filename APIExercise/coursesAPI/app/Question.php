<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Question extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'Questions';
    protected $primaryKey = 'id_question';
    protected $fillable = [
        'id_lesson', 'id_course', 'id_type', 'question', 'score', 'created_at', 'updated_at'
    ];

    public function question_type(){
        return $this->belongsTo('App\Question_type');
    }
    public function course(){
        return $this->belongsTo('App\Course');
    }
    public function lesson(){
        return $this->belongsTo('App\Lesson');
    }
    public function answers(){
        return $this->hasMany('App\Answer');
    }


}

