<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsRecordStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions_record_students', function (Blueprint $table) {
            $table->bigIncrements('id_question_record');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_question');
            $table->unsignedBigInteger('id_lesson');
            $table->integer('obtained_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions_record_students');
    }
}
