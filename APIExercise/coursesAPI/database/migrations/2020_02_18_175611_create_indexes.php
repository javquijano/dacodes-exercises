<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function(Blueprint $table){
            $table->foreign('parent_course')->references('id_course')->on('courses')->onDelete('cascade');
        });
        Schema::table('lessons', function(Blueprint $table){
            $table->foreign('parent_lesson')->references('id_lesson')->on('lessons')->onDelete('cascade');
            $table->foreign('id_course')->references('id_course')->on('courses')->onDelete('cascade');
        });
        Schema::table('questions', function(Blueprint $table){
            $table->foreign('id_lesson')->references('id_lesson')->on('lessons')->onDelete('cascade');
            $table->foreign('id_course')->references('id_course')->on('courses')->onDelete('cascade');
            $table->foreign('id_type')->references('id_type')->on('question_types')->onDelete('cascade');
        });
        Schema::table('answers', function(Blueprint $table){
            $table->foreign('id_question')->references('id_question')->on('questions')->onDelete('cascade');
        });
        Schema::table('users', function(Blueprint $table){
            $table->foreign('id_user_type')->references('id_user_type')->on('user_types')->onDelete('cascade');
        });
        Schema::table('courses_record_students', function(Blueprint $table){
            $table->foreign('id_user')->references('id_user')->on('users')->onDelete('cascade');
            $table->foreign('id_course')->references('id_course')->on('courses')->onDelete('cascade');
        });
        Schema::table('lessons_record_students', function(Blueprint $table){
            $table->foreign('id_user')->references('id_user')->on('users')->onDelete('cascade');
            $table->foreign('id_lesson')->references('id_lesson')->on('lessons')->onDelete('cascade');
            $table->foreign('id_course')->references('id_course')->on('courses')->onDelete('cascade');
        });
        Schema::table('questions_record_students', function(Blueprint $table){
            $table->foreign('id_user')->references('id_user')->on('users')->onDelete('cascade');
            $table->foreign('id_lesson')->references('id_lesson')->on('lessons')->onDelete('cascade');
            $table->foreign('id_question')->references('id_question')->on('questions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indexes');
    }
}
