<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLessonsRecordStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons_record_students', function (Blueprint $table) {
            $table->bigIncrements('id_lesson_record');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_lesson');
            $table->unsignedBigInteger('id_course');
            $table->integer('status')->comment('0)inactive, 1)coursing, 2)approved, 3)failed');
            $table->integer('current_score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_lessons_students');
    }
}
