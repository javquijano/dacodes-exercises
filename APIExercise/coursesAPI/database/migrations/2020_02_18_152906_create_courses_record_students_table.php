<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesRecordStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_record_students', function (Blueprint $table) {
            $table->bigIncrements('id_enrollment');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_course');
            $table->integer('status')->comment('0)inactive, 1)coursing, 2)approved, 3)failed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrolled_students_courses');
    }
}
