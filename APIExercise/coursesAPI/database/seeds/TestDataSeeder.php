<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $courses = [
            [
                "course_name" => "test_course",
                "parent_course" => NULL,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "course_name" => "test_course2",
                "parent_course" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "course_name" => "test_course3",
                "parent_course" => 2,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ]
        ];

        $lessons = [
            [
                "lesson_name" => "test_lesson",
                "parent_lesson" => NULL,
                "id_course" => 1,
                "approval_score" => 200,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "lesson_name" => "test_lesson2",
                "parent_lesson" => 1,
                "id_course" => 1,
                "approval_score" => 200,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "lesson_name" => "test_lesson3",
                "id_course" => 2,
                "parent_lesson" => NULL,
                "approval_score" => 300,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "lesson_name" => "test_lesson4",
                "parent_lesson" => 3,
                "id_course" => 2,
                "approval_score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
        ];

        $questions = [
            [
                "question" => "test_question1",
                "id_course" => 1,
                "id_lesson" => 1,
                "id_type" => 1,
                "score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "question" => "test_question2",
                "id_course" => 1,
                "id_lesson" => 1,
                "id_type" => 2,
                "score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "question" => "test_question3",
                "id_course" => 1,
                "id_lesson" => 2,
                "id_type" => 1,
                "score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "question" => "test_question4",
                "id_course" => 1,
                "id_lesson" => 2,
                "id_type" => 1,
                "score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "question" => "test_question5",
                "id_course" => 2,
                "id_lesson" => 3,
                "id_type" => 3,
                "score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "question" => "test_question6",
                "id_course" => 2,
                "id_lesson" => 3,
                "id_type" => 4,
                "score" => 100,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
        ];

        $answers = [
            //----- test Boolean
            [
                "id_question" => 1,
                "answer_text" => "true",
                "answer_value" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 1,
                "answer_text" => "false",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            //----- test MC1
            [
                "id_question" => 2,
                "answer_text" => "answer1",
                "answer_value" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 2,
                "answer_text" => "answer2",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 2,
                "answer_text" => "answer3",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            //----- test MCM
            [
                "id_question" => 5,
                "answer_text" => "answer1",
                "answer_value" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 5,
                "answer_text" => "answer2",
                "answer_value" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 5,
                "answer_text" => "answer3",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 5,
                "answer_text" => "answer4",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            //----- test MCA
            [
                "id_question" => 6,
                "answer_text" => "answer1",
                "answer_value" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 6,
                "answer_text" => "answer2",
                "answer_value" => 1,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 6,
                "answer_text" => "answer3",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [
                "id_question" => 6,
                "answer_text" => "answer4",
                "answer_value" => 0,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
        ];

        DB::table('courses')->insert($courses);
        DB::table('lessons')->insert($lessons);
        DB::table('questions')->insert($questions);
        DB::table('answers')->insert($answers);
    }
}
