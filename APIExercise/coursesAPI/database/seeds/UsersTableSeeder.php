<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [//Professor account
                "user_firstname" => "admin",
                "user_lastname" => "seed",
                "email" => "admin@dacodes.com",
                "password" => Crypt::encrypt("admin"),
                "id_user_type" => 1,
                "api_token" => "admin_token",
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ],
            [//Student account
                "user_firstname" => "user",
                "user_lastname" => "seed",
                "email" => "user@dacodes.com",
                "password" => Crypt::encrypt("user"),
                "id_user_type" => 2,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
                "api_token" => null
            ]
        ];
        DB::table('Users')->insert($users);
    }
}
