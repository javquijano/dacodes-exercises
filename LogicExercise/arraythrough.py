## made with python 3.8.0
LOWER_LIMIT = 1
MATRIX_UPPER_LIMIT = 10000000000
TEST_UPPER_LIMIT = 5000

def matrix_exercise():
    sides = ["R", "D", "L", "U"]
    tests_number = get_number_test_arrays()
    array_sizes = get_matrices_sizes(tests_number)
    for element in array_sizes:
        value = spiral_matrix(element[0], element[1])
        print(sides[value - 1])


def get_number_test_arrays():
    try:
        num_arrays = int(input())
        if(num_arrays < LOWER_LIMIT or num_arrays > TEST_UPPER_LIMIT):
            print("Value out of range 1 - 5000")
            return get_number_test_arrays()
    except ValueError:
        print("invalid input element")
        return get_number_test_arrays()

    return num_arrays


def get_matrices_sizes(tests_number, array_sizes = []):
    try:
        for i in range(tests_number):
            n, m = input().split()
            n, m = int(n), int(m)
            n_limit = n < LOWER_LIMIT or n > MATRIX_UPPER_LIMIT
            m_limit = m < LOWER_LIMIT or m > MATRIX_UPPER_LIMIT
            if(n_limit or m_limit):
                print("Values out of range 1 - 10^9")
                return get_matrices_sizes(tests_number, array_sizes)
            tests_number -= 1
            array_sizes.append([n, m])
    except ValueError:
        print("invalid input element")
        return get_matrices_sizes(tests_number, array_sizes)

    return array_sizes


def spiral_matrix(total_rows, total_cols, side = -1, index_r = 0, index_c = 0):

    final_row = index_r >= total_rows
    final_col = index_c >= total_cols
    if final_row or final_col:
        return side

    side -= 2
    side += 1 if (total_rows - 1) != index_r else 0
    side += 1 if (total_cols - 1) != index_c else 0

    return spiral_matrix(total_rows - 1, total_cols - 1, side, index_r + 1, index_c + 1)


matrix_exercise()
